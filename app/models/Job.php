<?php

class Job extends \Eloquent{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_type';
}