<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    /**
     * @return mixed
     */
    public function records(){
        return $this->hasMany('Record');
    }

    public function department(){
        return $this->hasOne('Department', 'id', 'department_id');
    }

    /**
     *
     */
    public static function boot(){
        parent::boot();

        static::deleted(function($user){
            $user->records()->delete();
        });
    }

}
