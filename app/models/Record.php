<?php

class Record extends \Eloquent{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'records';

    protected $fillable = array(
        'user_id',
        'modifier_id',
        'asa_status_id',
        'surgical_procedures',
        'patient_name',
        'surgeon_name',
        'status_id',
        'date',
        'start_time',
        'end_time',
        'diagnosis',
        'facility_id',
        'call_id',
        'comments',
        'attachment',
        'signature'
    );

    public function user(){
        return $this->hasOne('User', 'id', 'user_id');
    }
    public function status(){
        return $this->hasOne('Status', 'id', 'status_id');
    }
    public function facility(){
        return $this->hasOne('Facility', 'id', 'facility_id');
    }
    public function call(){
        return $this->hasOne('Call', 'id', 'call_id');
    }
    public function job(){
        return $this->hasOne('Job', 'id', 'job_id');
    }
    public function modifier(){
        return $this->hasOne('Modifier', 'id', 'modifier_id');
    }
    public function asa_status(){
        return $this->hasOne('AsaStatus', 'id', 'asa_status_id');
    }
}