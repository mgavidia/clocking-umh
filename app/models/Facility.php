<?php

class Facility extends \Eloquent{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facility_type';
}