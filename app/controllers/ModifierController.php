<?php

class ModifierController extends \BaseController {

    /**
     * Get all
     */
    public function index()
    {
        try{
            $response = [
                'modifiers' => []
            ];
            $statusCode = 200;
            $mods = Modifier::all();

            foreach($mods as $mod){
                $response['modifiers'][] = [
                    'modifier' => [
                        'id' => $mod->id,
                        'name' => $mod->name
                    ]
                ];
            }

        } catch (Exception $e){
            $statusCode = 404;
        } finally {
            return Response::json($response, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $input = Input::all();
            $modifier = Modifier::find($id);
            $modifier->fill($input);
            $modifier->save();
            $response['message'] = "Modifier {$modifier->id} updated.";

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
            $response['message'] = "Couldn't update Modifier {$modifier->id}.";
        } finally {
            return Response::json($response, $statusCode);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $modifier = Modifier::find($id);
            $response['message'] = "Modifier {$modifier->id} Deleted";
            $modifier->delete();

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
        } finally {
            return Response::json($response, $statusCode);
        }
    }
}
