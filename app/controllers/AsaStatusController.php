<?php

class AsaStatusController extends \BaseController {

    /**
     * Get all
     */
    public function index()
    {
        try{
            $response = [
                'asa_statuses' => []
            ];
            $statusCode = 200;
            $statuses = AsaStatus::all();

            foreach($statuses as $status){
                $response['asa_statuses'][] = [
                    'asa_status' => [
                        'id' => $status->id,
                        'name' => $status->name
                    ]
                ];
            }

        } catch (Exception $e){
            $statusCode = 404;
        } finally {
            return Response::json($response, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $input = Input::all();
            $status = AsaStatus::find($id);
            $status->fill($input);
            $status->save();
            $response['message'] = "Asa Status {$status->id} updated.";

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
            $response['message'] = "Couldn't update Asa Status {$status->id}.";
        } finally {
            return Response::json($response, $statusCode);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $status = AsaStatus::find($id);
            $response['message'] = "Asa Status {$status->id} Deleted";
            $status->delete();

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
        } finally {
            return Response::json($response, $statusCode);
        }
    }
}
