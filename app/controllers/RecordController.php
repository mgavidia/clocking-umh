<?php

class RecordController extends \BaseController {
    /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        /*if (Auth::id() != $id){
            return Response::make('Unauthorized', 401);
        }*/
        // validate
        $input = Input::all();
        $error = 200;
        $message = '';
        $id = -1;
        try {
            $pending = Status::find(1);//Status::where('default', 'like', 1);
            $record = Record::create([
                'user_id' => $input['user'],
                'status_id' => $pending->id,
                'date' => $input['date'],
                'start_time' => $input['start'],
                'end_time' => $input['end'],
                'diagnosis' => $input['diagnosis'],
                'patient_name' => $input['patient'],
                'surgeon_name' => $input['surgeon'],
                'surgical_procedures' => $input['procedure'],
                'modifier_id' => $input['modifier_id'],
                'asa_status_id' => $input['asa_status_id'],
                'facility_id' => Facility::find(1)->id,
                'call_id' => Call::find(1)->id,
                'comments' => $input['notes'],
                'signature' => $input['signature'],
            ]);
            if (Input::hasFile('file')){
                $file = Input::file('file');
                $filename = $file->getClientOriginalName();
                $storage = $this->getStorage($record->user_id);
                $file->move($storage,$filename);
                $record->attachment = $filename;
            }
            $id = $record->id;
        } catch (Exception $ex){
            $error = 401;
            $message = 'Record create failure.';
        } finally{
            return Response::json([
                    'error' => !($error == 200),
                    'message' => $message,
                    'id' => $id
                ],
                $error
            );
        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        /*if (Auth::id() != $id){
            return Response::make('Unauthorized', 401);
        }*/
        try{
            $response = [
                'record' => []
            ];
            $statusCode = 200;

            $record = Record::find($id);

            $attachment = '';
            if (!empty($record->attachment)){
                $attachment = $this->getStorage($record->user_id) . $record->attachment;
            }

            $response['record'] = [
                'id' => $record->id,
                'date' => $record->date,
                'start' => $record->start_time,
                'end' => $record->end_time,
                'patient' => $record->patient_name,
                'diagnosis' => $record->diagnosis,
                'surgeon' => $record->surgeon_name,
                'procedure' => $record->surgical_procedures,
                'modifier_id' => $record->modifier_id,
                'asa_status_id' => $record->asa_status_id,
                'notes' => $record->comments,
                'attachment' => $attachment,
                'signature' => $record->signature
            ];
        }catch (Exception $e){
            $statusCode = 404;
        }finally{
            return Response::json($response, $statusCode);
        }
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $input = Input::all();
            $record = Record::find($id);
            $response['message'] = "Record {$record->id} updated.";
            if ($input['user']){
                $record->user_id = $input['user'];
            }
            if ($input['date']){
                $record->date = $input['date'];
            }
            if ($input['start']){
                $record->start_time = $input['start'];
            }
            if ($input['end']){
                $record->end_time = $input['end'];
            }
            if ($input['diagnosis']){
                $record->diagnosis = $input['diagnosis'];
            }
            if ($input['patient']){
                $record->patient_name = $input['patient'];
            }
            if ($input['surgeon']){
                $record->surgeon_name = $input['surgeon'];
            }
            if ($input['procedure']){
                $record->surgical_procedures = $input['procedure'];
            }
            if ($input['modifier_id']){
                $record->modifier_id = $input['modifier_id'];
            }
            if ($input['asa_status_id']){
                $record->asa_status_id = $input['asa_status_id'];
            }
            if ($input['notes']){
                $record->comments = $input['notes'];
            }
            if ($input['signature']){
                $record->signature = $input['signature'];
            }

            if (Input::hasFile('file')){

                $file = Input::file('file');
                $filename = $file->getClientOriginalName();
                if ($record->attachment != $filename) {
                    $storage = $this->getStorage($record->user_id);
                    // Delete old
                    if (file_exists($storage . $record->attachment)) {
                        unlink($storage . $record->attachment);
                    }
                    $file->move($storage, $filename);
                    $record->attachment = $filename;
                }
            }

            $record->save();

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
            $response['message'] = "Problem updating Record {$record->id}.";
        } finally {
            return Response::json($response, $statusCode);
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $record = Record::find($id);
            $response['message'] = "User {$record->id} Deleted";
            $record->delete();

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
        } finally {
            return Response::json($response, $statusCode);
        }
	}

    private function getStorage($id){
        $storage = __DIR__.'/uploads/attachments/'.$id.'/';
        if (!file_exists($storage)){
            mkdir($storage);
        }
        return $storage;
    }
}
