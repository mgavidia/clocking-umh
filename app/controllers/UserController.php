<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        try{
            $response = [
              'users' => []
            ];
            $statusCode = 200;
            $users = User::all();

            foreach($users as $user){
                $response['users'][] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email
                ];
            }

        } catch (Exception $e){
            $statusCode = 404;
        } finally {
            return Response::json($response, $statusCode);
        }
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        return Response::json(array('Hello' => 'Hello'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
        return Response::json(array('Hello' => 'Hello'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        /*if (Auth::id() != $id){
            return Response::make('Unauthorized', 401);
        }*/
        try{
            $response = [
                'user' => []
            ];
            $statusCode = 200;

            $user = User::find($id);

            $records = [];
            try{
                $recs = Record::where('user_id', '=', $id)->orderBy('date', 'DESC')->get();
                foreach($recs as $record){
                    $records[] = [
                        'record' => [
                        'id' => $record->id,
                        'date' => $record->date,
                        'status' => $record->status->name
                        ]
                    ];
                }
            } catch (Exception $e){
            }

            $response['user'] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'department' => $user->department_id,
                'phone' => $user->phone,
                'supervisor' => $user->supervisor_name,
                'records' => $records
            ];

        }catch (Exception $e){
            $statusCode = 404;
        }finally{
            return Response::json($response, $statusCode);
        }
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
        return Response::json(array('Hello' => 'Hello'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $input = Input::all();
        $user = User::find($id);

        if (isset($input['phone'])){
            $user->phone = $input['phone'];
        }
        if (isset($input['email'])){
            $user->email = $input['email'];
        }
        if (isset($input['department'])){
            $user->department_id = $input['department'];
        }
        if (isset($input['supervisor'])){
            $user->supervisor_name = $input['supervisor'];
        }

        $user->save();
        
        return Response::json([
               'error' => false,
               'message' => 'Page Updated'],
               200
        );

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try{

            $user = User::find($id);
            $response['message'] = "User {$user->id} Deleted";
            $user->delete();

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
        } finally {
            return Response::json($response, $statusCode);
        }
	}
}
