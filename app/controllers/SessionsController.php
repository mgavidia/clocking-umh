<?php

class SessionsController extends \BaseController {

	/**
	 * Show the form for creating a new resource.
	 * GET /sessions/create
	 *
	 * @return Response
	 */
	public function create()
	{
        return Response::json([
                'error' => true,
                'message' => 'Forbidden. Please login.'
            ],
            401
        );
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /sessions
	 *
	 * @return Response
	 */
	public function store()
	{
        // validate
        $input = Input::all();
        $error = 200;
        $message = '';
        $id = -1;
        try {
            $result = Auth::attempt([
                'email' => $input['email'],
                'password' => $input['password'],
            ]);
            if ($result) {
                $message = 'Login success.';
                $id = Auth::id();
                $error = 200;
            } else {
                $message = 'Login failure.';
                $error = 401;
                $id = -1;
            }
        } catch (Exception $ex){
            $error = 401;
            $message = 'Login failure.';
            $id = -1;
        } finally{
            return Response::json([
                    'error' => !($error == 200),
                    'message' => $message,
                    'id' => $id
                ],
                $error
            );
        }
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /sessions/{id}
	 *
	 * @return Response
	 */
	public function destroy()
	{
		//Auth::logout();
        return Response::json([
                'error' => false,
                'message' => 'Logout success.'
            ],
            200
        );
	}

    public function __call($method, $paramaters = array()){
        return Response::json([
                'error' => true,
                'message' => 'Page Not Found.'
            ],
            404
        );
    }

}