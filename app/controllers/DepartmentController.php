<?php

class DepartmentController extends \BaseController {

    /**
     * Get all
     */
    public function index()
    {
        try{
            $response = [
                'departments' => []
            ];
            $statusCode = 200;
            $depts = Department::all();

            foreach($depts as $dept){
                $response['departments'][] = [
                    'department' => [
                        'id' => $dept->id,
                        'code' => $dept->code,
                        'description' => $dept->description
                    ]
                ];
            }

        } catch (Exception $e){
            $statusCode = 404;
        } finally {
            return Response::json($response, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $input = Input::all();
            $department = Department::find($id);
            $department->fill($input);
            $department->save();
            $response['message'] = "Department {$department->id} updated.";

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
            $response['message'] = "Couldn't update Department {$department->id}.";
        } finally {
            return Response::json($response, $statusCode);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $statusCode = 200;
        $response = [
            'error' => false
        ];
        try {
            $department = Department::find($id);
            $response['message'] = "User {$department->id} Deleted";
            $department->delete();

        } catch (Exception $e){
            $statusCode = 404;
            $response['error'] = true;
        } finally {
            return Response::json($response, $statusCode);
        }
    }
}
