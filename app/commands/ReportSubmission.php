<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ReportSubmission extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'clocking:submission';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Polls the pending records and calculates their time, preparing a report for submission and finally setting their status to submitted.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

    private function fixTime(&$array)
    {
        $hours = $array['hours'];
        $mins = $array['mins'];
        $array['hours'] = $hours + (integer)($mins/60);
        $array['mins'] = (integer)($mins%60);
    }

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        if ($this->option('simulate')){
            $this->comment('Simulating submission process.');
        }
        $this->info('Retrieving records...');
        $records = Record::orderBy('user_id', 'ASC')->get();
        $currentUser = array('id' => -1, 'hours' => 0, 'mins' => 0);
        $report = array(sizeof($records));
        foreach($records as $record){
            if ($currentUser['id'] != $record->user_id){
                if ($currentUser['id'] == -1){
                    $currentUser['id'] = $record->user_id;
                } else {
                    $this->comment('  └--Total hours: ' . $currentUser['hours'] . ' hours and ' . $currentUser['mins'] . ' minutes');
                    $report[] = $currentUser;
                    $currentUser = array('id' => $record->user_id, 'hours' => 0, 'mins' => 0);
                }
                $this->comment('Processing user ' . $currentUser['id'] . ' records.');
            }
            if (Status::find($record->status_id)->default){
                $start = new DateTime($record->start_time);
                $end = new DateTime($record->end_time);
                $diff = $start->diff($end);
                $hours = $diff->format('%h');
                $mins = $diff->format('%i');
                $this->comment('  └--Record id [' . $record->id . '] hours elapsed: ' . "$hours hours and $mins minutes");
                $currentUser['hours'] += intval($hours);
                $currentUser['mins'] += intval($mins);
                $this->fixTime($currentUser);
                if (!$this->option('simulate')){
                    // Process records
                    $record->status_id = Status::where('name', 'Completed')->first()->id;
                    $record->save();
                }
            }
        }
        $this->fixTime($currentUser);
        $report[] = $currentUser;
        $this->comment('  └--Total hours: ' . $currentUser['hours'] . ' hours and ' . $currentUser['mins'] . ' minutes');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			/*array('example', InputArgument::REQUIRED, 'An example argument.'),*/
		);
	}

	/**
	 * Get the console command options.
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('simulate', null, InputOption::VALUE_NONE, 'Simulate submission without processing records.', null),
		);
	}

}
