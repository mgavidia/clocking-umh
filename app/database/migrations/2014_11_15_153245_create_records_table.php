<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('records', function($table){
            $table->increments('id');
            // Foreign key
            $table->integer('user_id');
            // Foreign key
            $table->integer('status_id');
            $table->date('date');
            $table->time('start_time');
            $table->time('end_time');
            // Foreign key
            $table->integer('modifier_id');
            // Foreign key
            $table->integer('asa_status_id');
            $table->string('diagnosis');
            $table->string('surgical_procedures')->nullable();
            $table->String('patient_name')->nullable();
            $table->string('surgeon_name')->nullable();
            // Foreign key
            $table->integer('facility_id');
            // Foreign key
            $table->integer('call_id');
            // Foreign key
            $table->integer('job_id')->nullable();
            $table->text('comments');
            $table->string('attachment')->nullable();
            $table->string('signature');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('records');
	}

}
