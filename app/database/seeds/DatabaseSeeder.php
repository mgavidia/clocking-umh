<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('ApplicationSeeder');
	}

}

class ApplicationSeeder extends Seeder {
    public function run(){
        DB::table('users')->delete();

        $department1 = Department::create([
            'code' => 'RAD',
            'description' => 'Radiology'
        ]);

        $department2 = Department::create([
            'code' => 'ER',
            'description' => 'Emergency Room'
        ]);

        $department3 = Department::create([
            'code' => 'IM',
            'description' => 'Internal Medicine'
        ]);

        $user1 = User::create([
            'email' => 'test1@miami.edu',
            'name' => 'Test User1',
            'password' => Hash::make('test1'),
            'department_id' => $department3->id,
            'phone' => '305-284-0000',
            'supervisor_name' => 'Dr. Supervisor'
        ]);
        $user2 = User::create([
            'email' => 'test2@miami.edu',
            'name' => 'Test User2',
            'password' => Hash::make('test2'),
            'department_id' => $department2->id,
            'phone' => '305-284-0000',
            'supervisor_name' => 'Dr. Supervisor'
        ]);

        $status_pending = Status::create([
            'name' => 'Pending',
            'default' => true
        ]);

        $status_processed = Status::create([
            'name' => 'Completed',
            'default' => false
        ]);

        $facility_jmh = Facility::create([
            'code' => 'JMH',
            'description' => 'Jackson Memorial Hospital'
        ]);

        $facility_umh = Facility::create([
            'code' => 'UMH',
            'description' => 'University of Miami Hospital'
        ]);

        $facility_bpei = Facility::create([
            'code' => 'BPEI',
            'description' => 'Bascom Palmer Eye Institute'
        ]);

        $call_cvt = Call::create([
            'code' => 'CVT',
            'description' => 'CVT'
        ]);

        $call_ob = Call::create([
            'code' => 'OB',
            'description' => 'OB'
        ]);

        $asa_status = AsaStatus::create(['name' => '1']);
        AsaStatus::create(['name' => '2']);
        AsaStatus::create(['name' => '3']);
        AsaStatus::create(['name' => '4']);
        AsaStatus::create(['name' => '5']);
        AsaStatus::create(['name' => '6']);
        AsaStatus::create(['name' => 'E']);

        $modifier = Modifier::create(['name' => '22']);
        Modifier::create(['name' => '23']);
        Modifier::create(['name' => '25']);
        Modifier::create(['name' => '59']);
        Modifier::create(['name' => 'Other']);
        // User 1 records
        for ($i=0; $i<20; $i++){
            $this->createRecord($user1->id);
        }
        // User 2 records
        for ($i=0; $i<20; $i++){
            $this->createRecord($user2->id);
        }
    }

    private function createRecord($user_id){
        $month = rand(1,12);
        $date = '2014-' . strval($month) . '-' . strval(rand(1, 28));
        $start = strval(rand(6,12)) . ':' . strval(rand(0,59)) . ':00';
        $end = strval(rand(13,23)) . ':' . strval(rand(0,59)) . ':00';
        Record::create([
            'user_id' => $user_id,
            'surgical_procedures' => 'None',
            'patient_name' => 'N/A',
            'surgeon_name' => 'N/A',
            'status_id' => $month < 7 ? 2 : 1,
            'date' => new DateTime($date),
            'start_time' => new DateTime($date . ' ' . $start),
            'end_time' => new DateTime($date . ' ' . $end),
            'modifier_id' => rand(1, 5),
            'asa_status_id' => rand(1, 7),
            'diagnosis' => 'Sick',
            'facility_id' => 1,
            'call_id' => 1,
            'comments' => 'Long shift today.',
            'signature' => 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARgAAACWCAYAAAAFQ5eQAAAPVUlEQVR4Xu2dB8g8RxmHjbF3xF4v2HvEihj91AQVOyoGI/jZI7YoiqIYPwuoKBbsYvnEFrHF3vWzK4rG3s1ZYyzYu1HfB29hXO5u93Zv/jc3eQZebu9ud/adZ2Z/O21nDzuLQQISkEAmAodlitdoJSABCZxFgbEQSEAC2QgoMNnQGrEEJKDAWAYkIIFsBBSYbGiNWAISUGAsAxKQQDYCCkw2tEYsAQkoMJYBCUggGwEFJhtaI5aABBQYy4AEJJCNgAKTDa0RS0ACCoxlQAISyEZAgcmG1oglIAEFxjIgAQlkI6DAZENrxBKQgAJjGZCABLIRUGCyoTViCUhAgbEMSEAC2QgoMNnQGrEEJKDAWAYkIIFsBBSYbGiNWAISUGAsAxKQQDYCCkw2tEYsAQkoMJYBCUggGwEFJhtaI5aABBQYy4AEJJCNgAKTDa0RS0ACCoxlQAISyEZAgcmG1oglIAEFxjIgAQlkI6DAZENrxBKQgAJjGZCABLIRUGCyoTViCUhAgbEMSEAC2QgoMNnQGrEEJKDAWAYkIIFsBBSYbGiNWAISUGAsAxKQQDYCCkw2tEYsAQkoMJYBCUggGwEFJhtaI5aABBQYy4AEJJCNgAKTDa0RS0ACCoxlQAISyEZAgcmG1oglIAEFxjIgAQlkI6DAZENrxBKQgAJjGZCABLIRUGCyoTViCUigdIGZRBZh0ySr2OY3wo3D/hN2nrDzhp0a9o3W/uayBCSwIQIlCsxOsHhR2NUHMvlLHHe/sJMGHu9hEpDAmgiUJDDHRZqeltROxiTxX3HwMWEHYyLxWAlIYByBTQvMhcL9T4+orSxL/Z/iz8uG/W4cIo+WgASGEtiEwNAEem7YkWFnhB0+1Pkex70s9jm+x37uIgEJZCBwKAUmFZaupHwndrjNbKdJfE6TA9hufjt29judvNcJe1hYmib2PaLrZP4vAQnkIXAoBIZm0NvDEJiu8JPY4cSw/a4dF/x/Wvx+ieS/n8Y2zSSDBCSwAQK5BebakaaPhF2kI22vif/3wqYjGXw5jqfp1YRTYuO6I+P0cAlIYCCBXAJz5/DnSa2LPXXxrfHl0WsQlHayvxk/XC358VuxPXS4eyBSD5OABBoC6xaYrubQD+LER2cQliY9P4qNy1mDsYBLoAwC6xSYSSTp/WFXWZC0r8TvO2E5ho2PinjfG3a+1rlfFd+ZdGeQgAQ2QGBdAvO88P3+YUzXb4d3xQ/PCTvIlD76XD4fdo458X8ifrt5pvMarQQk0EFgrMDQJPpC2BXnnIdngu4VRkdrrkBfz+vDGKaeF74YP94g18mNVwISWE5grMBQK2nXEHj4kAluD84IfyfifkHYNTvOwXyaq2b0w6glIIElBMYIzMkR751acb8hvj8kLEc/C6eahD0r7G4L0oS4pWn6WHy/pSVAAhLYDIGhAkPT6Lctl58f30/IkAxqK9SSqK0sEhZOe3rYL8OulfhA3wxLOhgkIIENEBgqMC8MX6mpNOHnsXHpNftP/8prw9ojQ+3TICxPCXtx2OfCbqTArDknjE4CAwkMFZjvx/mukJxzzGgNtSGaWowGUUO5TNgfwi7QkSZqJ48LO0j2e2Vs3zf5zpPaNx3IxsMkIIGRBIYKDCNH10/O/bXY5rGAVQLC8ogwZvR21VKaeP8YGz8M2w1rj04R34/Dzp848fXYTptMq/jnvhKQwEgCQwXmF3HeiyfnpnP1qWGvDpvOfp/MPnlkgJoJIvLt2Sf7UGPpIyw8X0QTiI7jgyXpZS4OgpWGZ8eXx4xk5OESkMBAAkMFhhm7tx54zmWH0Z9yrrALznZ6R3zSF9MnIHJp4LGBSZ8D3UcCEshDYKjAcOEyx2Te7NlVPf1zHPCWsL2w6aoHz/bfjU9qT034a2xcKizXcPlANz1MAmcuAkMFBko7YSzFcNYeyFgjl9oJn5ecCROdtHTmPnCEsHBq+l5+FXa2xA86e3l0wSABCWyQwBiBwW3EgVm7y8Jn4897jhSRZfEz94YlOJvwt9g49waZemoJSGBGYKzAEM0krJnMxjyU6Sxufm+2cwL/e0SeNtV45clDc55wDXFT62KWMR3dDPnfNeyra4jXKCRQFIF1CMwmE0QHMMtxNoH+HObRlNz3griwEFa6tCciyRrEB5uE6bklsG4C2y4w7eHyRwYghqtLDYjLd8MuOsdB1suhRmOQQDUEtllgdiMX0pEjFgxPV7MrKZMQlveFLXsu6vfxP/sZJFANgW0WGEaO0sXEmdDHhLzSwiQc+mDYlTocOzPPOuZxDp5lYy7TxcJo6vIKYEYap6VlqP70J7CtAtPue+EtjukjAv0J5N1zL6JnJvG8BbH+Hb+nQ/xntomB1NaeEMa7rM65IBvIVyZ18tYJXkGD2AztX7t9HMuKi82SHh8YEVfeUlNR7NsoMJPgz+MDaXNibN/LTsRHpyuFL+0foeDzBgQK9qrhnXHAHRYcRHOONNwx+f+Nsc1wfm2BfOKleDBGHChzlw9rHmxdNb1MQ2BZDkYO2SZvJrNt+uSabR5DwRY1OxEvHiV58qoOuH9/AtsoMO13H63yOEFDhsJO4WNVPApkV7hL7HBy107J//uxfe85+yMij59dFO3HLbijNm+zXOFURe4KU54Lo6ZJ0+fsRXr5P6eYJnDMLE8KdnM7Xds2gZk3LM1CVNMe+LmTsXzDkPck/TOO4+nxrrkqnOOTYe2lPJmMyNISafX+GfH9sYnfz5zt0yMpRe5C2lkYbHcmLH2dZHZ3Ogu773Hr3o8mGFMc+OTRlY+HrXJTWbc/VcS3bQLzpaCevqnxAfH9FUtyIp3QNjbD/hERsJjVKQsi4lzzXpG7aKU/9k0f5KQwU1MaEppOUo49lB2jOzNRIR3XCGvXVOhXYvTsPWE0SWBELQ2hZVLmwWybpT4Q/rSTl+Yj/SaHt4AwZ+h7YTRl+zSRaEoNndn96ziWzvfJzH++E5rBBb4385mokVI2qFH36SciTpqKMJq20ljN120RGArmh8LSNWiYT7LoHUxNBrE8RNc+q2TmsgWsuOPdrBXZsoW4eLzhhGR/5u/Ql9QnUDhZVZD1huddPJ+K3+n/6VPQ+5yv2SetpezEj/P6NxhuRywbWyX+eftyHgJpmY5IE/EgBgjdkWHM9k6XHBnrZ3o8/D8cdmwYIojviBI1NQYj8KMRTkbL6MR+ediim9c6fTukcW2LwHDxtdd6IfPetIQWL2K7bQ+aP4t9WPSqqSI3nbwXjt8Y+k4ZMfJz4qwg0IfDcCrh6WHtZhGCw5190UX+7vjvdol/3OW5Y3eFvdjhUWFdo2Y0624487UrzkX/78YfTQctF+WigKggvixxyp183cI21P+u48gfeJJGAulAEHIJT5c/94kd9rt22qb/t0FgKNh07KZh2YgLhYbnkViuYV7gQqD/gzvZdGaL8oz3Kl1vQIbyTiiaLcsuNO5wt0ri7urkpbbwtrBbrOAPy1Ywua+r7yiNEuFmrZ/djvNQtaeWcjD7XMGt4nfdCQ/3wlibqGkCITyMUhGaJhJ5SNOOm9GQctIGQQ0m7QIoHlSXg9sgMB+NRKQXFQUb0UkvXi4+RIW1fee9XZKLYJULs+FGQePCX2XdG54z6tOR3G4i0bF49zDSQjp2w6glIYSsT0wHJJPQ0tCeSzMvv+k7emIYfTOERW1+hBmfJvMimf2GOO+H4eu21FKWJGetf5FvO2EINPnCDQ6W5B99NWkTiRpSu28JZ6p7XKR0gXlJQD++VQxQ+LStSqbydsdFNZaxozPURLhTLZoMlrpHFRt/utrSFMZpWLNyH3HwGhiaXMvmbjTnOiM2mMPByBQBHxEgRJSO7658pdDTT0BHK2J95bB5okh6DsKamgo+G9ZDYN5SJzaR1sO2VyzHxV6va+3Jq2gRkybQN/PwJRcUk6j2ep1t+U5chJ8JWzYacVr8f5OwPhchPtG/MyR0idgkIqVzu48gLjo/d1J8PAizpjIkl/odQ14dHcYNh5tY142pX6wF7dV1p9uUqwBnSPqIxAHuuEcl3+d1/DZzKrhATphdIOtKAz4xxErVlou8ab706ctp+8DF20dgfhP7MUzK/BI6o6m17Pe46BFE+o/mVcOX8SBd+AZbgwRGEyhVYNovUCOhaW1knrh0zYkZDWuNESBWjFrN6y/iNDyewMjWdMQ559UAm+hoiiHYaSfmS+M7TaEx5xzhrofWSKBEgZl3YVAj2QnjzszrUdovU1tXU+hQ5jHpYag6FRmGtvfCDtbkyCTiYfic2lYzAoKAKCJrAmw0ywmUJjDc2U8N47MJVNsZVn5QGBdMOzBJaXdLM5r0zFtudEuTo9sS+H8CpQnMfrjXfkiQTsZUcJoUUM3nMQH6WgwSkECBBEoSGESEodquwPuYGJ49CHOEo4uW/0tggwRKEhhWo2NC2KJAPwydu/sb5OWpJSCBFQiUIjD3CJ9PWuI3q8IxRGuQgAS2iEAJArNMXOjg3Q1j+NQgAQlsGYFNC8wkeC16xzXPzOyETbeMqe5KQAIzApsWmHkT5nBt7PNDZrAEJFAAgdIEhqdQeRr1zQWw0QUJSGAkgU0LDE2kgzCWDjw9jElnNolGZqqHS6AUApsWmIYDQqOwlFIq9EMCayJQisCsKTlGIwEJlERAgSkpN/RFApURUGAqy1CTI4GSCCgwJeWGvkigMgIKTGUZanIkUBIBBaak3NAXCVRGQIGpLENNjgRKIqDAlJQb+iKByggoMJVlqMmRQEkEFJiSckNfJFAZAQWmsgw1ORIoiYACU1Ju6IsEKiOgwFSWoSZHAiURUGBKyg19kUBlBBSYyjLU5EigJAIKTEm5oS8SqIyAAlNZhpocCZREQIEpKTf0RQKVEVBgKstQkyOBkggoMCXlhr5IoDICCkxlGWpyJFASAQWmpNzQFwlURkCBqSxDTY4ESiKgwJSUG/oigcoIKDCVZajJkUBJBBSYknJDXyRQGQEFprIMNTkSKImAAlNSbuiLBCojoMBUlqEmRwIlEVBgSsoNfZFAZQQUmMoy1ORIoCQCCkxJuaEvEqiMgAJTWYaaHAmURECBKSk39EUClRFQYCrLUJMjgZIIKDAl5Ya+SKAyAgpMZRlqciRQEgEFpqTc0BcJVEZAgaksQ02OBEoioMCUlBv6IoHKCCgwlWWoyZFASQQUmJJyQ18kUBkBBaayDDU5EiiJgAJTUm7oiwQqI6DAVJahJkcCJRFQYErKDX2RQGUE/gv1heemHsAqxAAAAABJRU5ErkJggg=='
        ]);
    }
}
