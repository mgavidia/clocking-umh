<?php

                                           /*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['before' => 'auth', 'as' => 'home', 'uses' => 'UserController@index']);

/*Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});*/

Route::get('login', 'SessionsController@create');
Route::get('logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController', ['only' => ['create', 'store', 'destroy']]);

Route::group(array('prefix' => 'api/v1', ), function()
//Route::group(array('prefix' => 'api/v1', 'before' => 'auth'), function()  // oauth maybe later
{
    Route::resource('users', 'UserController', ['except' => ['index', 'destroy']]);
    Route::resource('records', 'RecordController', ['only' => ['store', 'show', 'update', 'destroy']]);
    Route::resource('departments', 'DepartmentController', ['only' => ['index', 'store', 'update', 'destroy']]);
    Route::resource('modifiers', 'ModifierController', ['only' => ['index']]);
    Route::resource('asa_statuses', 'AsaStatusController', ['only' => ['index']]);
});