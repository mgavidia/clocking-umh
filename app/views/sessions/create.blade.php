<h1>Login</h1>
@if (Session::get('flash_message'))
    <div>
        {{ Session::get('flash_message') }}
    </div>
@endif
{{ Form::open(array('action' => 'SessionsController@store')) }}
    <ul>
        <li>
            {{ Form::label('email', 'Email:') }}
            {{ Form::text('email') }}
        </li>
        <li>
            {{ Form::label('password', 'Password:') }}
            {{ Form::password('password') }}
        </li>
        <li>
            {{ Form::submit() }}
        </li>
    </ul>
{{ Form::close() }}