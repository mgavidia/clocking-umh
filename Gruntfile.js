/**
 * Created by mgavidia on 11/18/14.
 */
module.exports = function(grunt) {
    grunt.initConfig({
        // Paths variables
        paths: {
            // Development where put SASS files, etc
            assets: {
                css: './assets/sass/',
                js: './assets/js/',
                vendor: './assets/vendor/'
            },
            // Production where Grunt output the files
            css: './public/css/',
            js: './public/js/'

        },
        pkg: grunt.file.readJSON('package.json'),
        requirejs: {
            compile: {
                options: {
                    name: "main",
                    baseUrl: "assets/js/",
                    mainConfigFile: "js/main.js",
                    out: "assets/dist/compiled.js"
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: '<%= paths.assets.js %><%= pkg.name %>.js',
                dest: '<%= paths.js %><%= pkg.name %>.min.js'
            }
        }
    });

    // !! This loads the plugin into grunt
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['concat:js', 'uglify:js', 'require:js']);

};