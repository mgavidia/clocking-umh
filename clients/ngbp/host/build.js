/**
 * Created by mgavidia on 12/9/14.
 */

angular.module( 'clocking.host', [] )

    .service('host', function(){
        return {
            get: function(){
                return 'http://localhost:8000/';
            }
        };
    })

;