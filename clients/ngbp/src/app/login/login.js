angular.module( 'ngBoilerplate.login', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'login', {
    url: '/login',
    views: {
      "main": {
        controller: 'LoginCtrl',
        templateUrl: 'login/login.tpl.html'
      }
    },
    data:{ pageTitle: 'Logon' }
  });
})

.controller( 'LoginCtrl', function LoginCtrl( $scope, $http, session, $state, alert, $timeout, $location, cors) {
    $scope.submit = function(){
        $http.post( cors.getHost() + 'sessions', {email: $scope.email, password: $scope.password}).
        success(function(data, status, headers, config){
            session.set({logon: true, id: data.id});
            alert.notify('alert-success', 'Login ok.', $timeout);
            $location.path('/home');
        }).error(function(data, status, headers, config){
            alert.notify('alert-danger', 'Credentials incorrect', $timeout);
        });
    };
})

;
