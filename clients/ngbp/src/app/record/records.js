angular.module( 'ngBoilerplate.records', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider, $urlRouterProvider ) {
  $stateProvider.state( 'records', {
    url: '/records',
    views: {
      "main": {
        controller: 'RecordCtrl',
        templateUrl: 'record/records.tpl.html'
      }
    },
    data:{ pageTitle: 'View Records' }
  });
})

.controller( 'RecordCtrl', function RecordCtrl( $scope, $timeout, cors, session, alert, $http ) {
    $scope.id = session.get().id;
    var api = cors.getApi() + 'users/' + $scope.id;
    $http.get(api, cors.get())
        .success(function(data, status, headers, config){
            $scope.records = data.user.records;

        }).error(function(data, status, headers, config){
            alert.notify('alert-danger', 'Error trying to retrieve content.', $timeout);
        });
    $scope.getStatus = function(id){
        if (id == 'Completed'){
            return 'success';
        }
        return 'warning';
    };
})

;
