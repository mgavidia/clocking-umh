angular.module( 'ngBoilerplate.add', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'add', {
    url: '/records/add',
    views: {
      "main": {
        controller: 'AddRecordCtrl',
        templateUrl: 'record/record.tpl.html'
      }
    },
    data:{ pageTitle: 'Add new record.'}
  });
})

.controller( 'AddRecordCtrl', function AddRecordCtrl( $scope, session, cors, $http, lists, alert, $location) {
    $scope.recordButton = 'Create Record';
    var currentDate = new Date();
    currentDate.setHours(currentDate.getHours(), currentDate.getMinutes(), 0,0);
    $scope.date = currentDate;
    $scope.start = currentDate;
    $scope.end = currentDate;
    $scope.modifier_id = 1;
    $scope.asa_status_id = 1;
    $scope.modList = lists.modifiers();
    $scope.asaList = lists.asa_statuses();
    var signature = new SignaturePad(document.querySelector('canvas'));
    $scope.resetSignature = function(){
        signature.clear();
    };
    $scope.submit = function(){
        $http.post(cors.getApi() + 'records', {
                user: session.get().id,
                date: $scope.date,
                start: $scope.start,
                end: $scope.end,
                patient: $scope.patient,
                surgeon: $scope.surgeon,
                diagnosis: $scope.diagnosis,
                procedure: $scope.procedure,
                modifier_id: $scope.modifier_id,
                asa_status_id: $scope.asa_status_id,
                notes: $scope.notes,
                file: $scope.fileUpload,
                signature: signature.toDataURL()
            }).success(function(data, status, headers, config){
                alert.notify('alert-success', 'Record added.');
                $location.path('records/edit/' + data.id);
            }).error(function(data, status, headers, config){
                alert.notify('alert-danger', 'Could not add record.');
            });
    };
})

;
