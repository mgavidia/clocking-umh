angular.module( 'ngBoilerplate.edit', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider
      .state( 'edit', {
        url: '/records/edit/:recordID',
        views: {
          "main": {
            controller: 'EditRecordCtrl',
            templateUrl: 'record/record.tpl.html'
          }
        },
        data:{ pageTitle: 'Edit existing record.' }
      });
})

.controller( 'EditRecordCtrl', function EditRecordCtrl( $scope, session, $timeout, $stateParams, cors, $http, lists, alert, $location) {
    $scope.recordID = $stateParams.recordID;
    $scope.recordButton = 'Save';
    $scope.modList = lists.modifiers();
    $scope.asaList = lists.asa_statuses();
    var signature = new SignaturePad(document.querySelector('canvas'));
    var api = cors.getApi() + 'records/' + $scope.recordID;
    $http.get(api, cors.get())
        .success(function(data, status, headers, config){
            var record = data.record;
            $scope.date = new Date(record.date);
            var time = new Date(record.start);
            time.setHours(time.getHours(), time.getMinutes(), 0,0);
            $scope.start = time;
            time = new Date(record.end);
            time.setHours(time.getHours(), time.getMinutes(), 0,0);
            $scope.end = time;
            $scope.patient = record.patient;
            $scope.surgeon = record.surgeon;
            $scope.diagnosis = record.diagnosis;
            $scope.procedure = record.procedure;
            $scope.modifier_id = record.modifier_id;
            $scope.asa_status_id = record.asa_status_id;
            $scope.notes = record.notes;
            $scope.attachment = record.attachment;
            signature.fromDataURL(record.signature);
        }).error(function(data, status, headers, config){
            alert.notify('alert-danger', 'Error trying to retrieve user content.');
    });
    $scope.resetSignature = function(){
        //console.log(signature.toDataURL());
        signature.clear();
    };
    $scope.submit = function(){
        // Save
        $http.put(api, {
                _method: 'PUT',
                user: session.get().id,
                date: $scope.date,
                start: $scope.start,
                end: $scope.end,
                patient: $scope.patient,
                surgeon: $scope.surgeon,
                diagnosis: $scope.diagnosis,
                procedure: $scope.procedure,
                modifier_id: $scope.modifier_id,
                asa_status_id: $scope.asa_status_id,
                notes: $scope.notes,
                file: $scope.fileUpload,
                signature: signature.toDataURL()
            }).success(function(data, status, headers, config){
                alert.notify('alert-success', 'Record successfully saved.');
                $location.path('records/edit/' + $scope.recordID);
            }).error(function(data, status, headers, config){
                alert.notify('alert-danger', 'Error trying to save record.');
            });
    };
})

;
