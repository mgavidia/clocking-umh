angular.module( 'ngBoilerplate.account', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'account', {
    url: '/account',
    views: {
      "main": {
        controller: 'AccountCtrl',
        templateUrl: 'account/account.tpl.html'
      }
    },
    data:{ pageTitle: 'My Information' }
  });
})

.controller( 'AccountCtrl', function AccountCtrl( $scope, session, $http, cors, alert, lists) {
    $scope.id = session.get().id;
    $scope.depts = lists.departments();
    var api = cors.getApi() + 'users/' + $scope.id;
    $http.get(api, cors.get())
        .success(function(data, status, headers, config){
            var user = data.user;
            $scope.name = user.name;
            $scope.email = user.email;
            $scope.department = user.department;
            $scope.phone = user.phone;
            $scope.supervisor = user.supervisor;
        }).error(function(data, status, headers, config){
            alert.notify('alert-danger', 'Error trying to retrieve user content.');
        });
    $scope.submit = function(){
      // Save
        $http.put(api, {_method: 'PUT', email: $scope.email, phone: $scope.phone, department: $scope.department, supervisor: $scope.supervisor})
            .success(function(data, status, headers, config){
                alert.notify('alert-success', 'User record successfully saved.');
                $location.path('account');
            }).error(function(data, status, headers, config){
                alert.notify('alert-danger', 'Error trying to save user content.');
            });
    };
})

;
