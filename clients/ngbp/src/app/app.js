angular.module( 'ngBoilerplate', [
  'ngCookies',
  'templates-app',
  'templates-common',
  'ngBoilerplate.home',
  'ngBoilerplate.add',
  'ngBoilerplate.edit',
  'ngBoilerplate.records',
  'ngBoilerplate.account',
  'ngBoilerplate.about',
  'ngBoilerplate.login',
  'ui.router',
  'clocking.host'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise( '/home' );
})

.run( function run ($rootScope, $location, session) {
    $rootScope.$on("$routeChangeStart", function(event, current, previous, rejection){
       //console.log(current, previous);
    });
    $rootScope.$on("$locationChangeStart", function (event, next, current) {
        if (!session.isLoggedIn()) {
            $location.path('/login');
        }
    });
})

.service('session', function($cookieStore){
    var info = {
        logon: false,
        id: -1
    };

    return {
        get: function(){
            return info;
        },
        set: function(values){
            _.extend(info, values);
        },
        isLoggedIn: function(){
            return info.logon;
        }
    };
})

.service('cors', function(host){
    /*var config = {
        withCredentials: true
    };*/
    var config = {};
    return {
        getHost: function(){
            return host.get();
        },
        getApi: function(){
            return this.getHost() + 'api/v1/';
        },
        get: function(){
            return config;
        }
    };
})

.service('alert', function($timeout){
    var alertInfo = {
        active: false,
        type: '',
        message: ''
    };
    return {
        get: function(){
            return alertInfo;
        },
        set: function(values){
            _.extend(alertInfo, values);
        },
        notify: function(type, message){
            this.set({active: true, type: type, message: message});
            $timeout(function(){
                _.extend(alertInfo, {active: false, type: '', message: ''});
            }, 4500);
        }
    };
})

.service('lists', function(cors, $http, $timeout){
    var lists = {
            departments: {},
            modifiers: {},
            asa_statuses: {}
    };
    return {
        init: function(){
            $http.get(cors.getApi() + 'departments', cors.get())
                .success(function(data, status, headers, config){
                    lists['departments'] = data.departments;
                }).error(function(data, status, headers, config){
                    alert.notify('alert-danger', 'Error trying to retrieve department content.', $timeout);
                });
            $http.get(cors.getApi() + 'modifiers', cors.get())
                .success(function(data, status, headers, config){
                    lists['modifiers'] = data.modifiers;
                }).error(function(data, status, headers, config){
                    alert.notify('alert-danger', 'Error trying to retrieve modifier content.', $timeout);
                });
            $http.get(cors.getApi() + 'asa_statuses', cors.get())
                .success(function(data, status, headers, config){
                    lists['asa_statuses'] = data.asa_statuses;
                }).error(function(data, status, headers, config){
                    alert.notify('alert-danger', 'Error trying to retrieve asa status content.', $timeout);
                });
        },
        set: function(values){
            _.extend(lists, values);
        },
        departments: function(){
            return lists.departments;
        },
        modifiers: function(){
            return lists.modifiers;
        },
        asa_statuses: function(){
            return lists.asa_statuses;
        }
    };
})


.controller( 'AppCtrl', function AppCtrl ( $scope, $location, $http, session, alert, cors, lists) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = 'Clocking UMH - ' + toState.data.pageTitle;
    }
  });
  // Init lists
  lists.init();
  $scope.isLoggedOn = function(){
      return session.isLoggedIn();
  };
  $scope.alertInfo = function(){
      return alert.get();
  };
  $scope.logout = function(){
      var redirect = false;
      $http.get(cors.getHost() + 'logout', cors.get())
      .success(function(data, status, headers, config){
          session.set({logon: false});
          alert.notify('alert-warning', 'You are logged out.');
          redirect = true;
      }).error(function(data, status, headers, config){
      });
      if (redirect){
          $location.path('/login');
      }
  };
  $scope.doCollapse = function(collapsed){
      if (window.innerWidth < 768){
          return !collapsed;
      }
      return true;
  };
})

;

