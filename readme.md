## Clocking UMH

*Restful services* implementation leveraging the [Laravel Framework](http://laravel.com). 

**See below for Laravel license information**

### Requirements
  - Install sqlite
  - Install php 5.x or above with mcrypt and sqlite mods
  - Install [composer](https://getcomposer.org/download/)

### Setup
  - Download and install all dependencies: `composer install`
  - Create database and tables: `./artisan migrate`
  - Seed the database with default data (optional): `./artisan db:seed`

### Usage
  - Run the development server: `./artisan serve`
  - To see existing routes: `/.artisan routes`
  - Access via: **http://localhost:8000/\[route\]**
    - *More useful if you use some rest service viewing tool*

### Laravel

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)